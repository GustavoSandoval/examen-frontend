import Vue from 'vue'
import VueRouter from 'vue-router'
import MainView from '../views/MainView.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainView
  },
  {
    path: '/recepcion',
    name: 'recepcion',
    component: () => import('../components/MostrarRecepciones.vue')
  },
  {
    path: '/crearProducto',
    name: 'crearProducto',
    component: () => import('../components/RegistrarProducto.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
